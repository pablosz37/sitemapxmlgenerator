class GeneratorController < ApplicationController
  def index
  end
  
  def out
    sites = Nokogiri::HTML(open(params[:html_link])).xpath('//div[@class="col-md-4 col-sm-6 col-xs-12"]//a')

    sitemap = Nokogiri::XML::Builder.new do
      urlset {
        sites.each do |site|
          url {
            loc "http://anxstrip.com"+site.attribute('href')
          }
        end
      }
    end
    
    render :xml => sitemap.to_xml
  end
end
